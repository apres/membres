# Droits et devoirs des membres de l'APRES

## Droits

- Accés à ce groupe de projets Git pour proposer des modifications des status et des coutumes, aka réglement intérieur.
- Accés à un salon exclusif sur le serveur Discord de l'association. Dans ce salon se trouve des liens **pour inviter d'autres personnes** sur le serveur.
- Droit d'édition sur le site internet de l'association pour écrire des articles sous leurs pseudonymes.
- Présence par défaut dans la liste des gens pouvant être tirer au sort pour faire parti du bureau. Un.e peut à n'importe quel moment demander à ne plus être inscrit sur cette liste.

## Devoirs

- Proposer un ou deux sujets par mois pour nos discussions hebdomadaires. Ceux-ci seront disponibles dans le salon #points-de-discussions du serveur discord.
- Essayer d'inviter une à deux personnes par mois à rejoindre le discord dans l'objectif de devenir membre de l'association.
